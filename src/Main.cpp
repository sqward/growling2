#include <osbind.h>
#include <mint/falcon.h>
#include <stdio.h>			/* printf */
#include <string.h>			/* strcpy */
#include <stdlib.h>			/* srand/rand */
#include <unistd.h>
#include <libmodplug/modplug.h>			/* core */

char *getFileData(char *filename, long *size)
{
    FILE *f;
    char *data;

    f = fopen(filename, "rb");
    if (f == NULL) {
      return NULL;
    }
    fseek(f, 0L, SEEK_END);
    (*size) = ftell(f);
    rewind(f);
    data = (char*)malloc(*size);
    fread(data, *size, sizeof(char), f);
    fclose(f);

    return(data);
}

enum  Buffer
{
	kBufferFirst = 0,
	kBufferSecond = 1
};

const size_t kBufferSize = 0x8000;
const size_t kBufferSizeHalf = kBufferSize / 2;

int main(int argc, char* argv[])
{
    ModPlug_Settings settings;
    ModPlug_GetSettings(&settings);

	    // Note: All "Basic Settings" must be set before ModPlug_Load.
    settings.mResamplingMode = MODPLUG_RESAMPLE_NEAREST; /* RESAMP */
    settings.mChannels = 2;
    settings.mBits = 16;
    settings.mFrequency = 50000;
   // settings.mStereoSeparation = 128;
   // settings.mMaxMixChannels = 256;
    /* insert more setting changes here */
    ModPlug_SetSettings(&settings);

    long size;
    char *filedata;
	
	// TODO: proper command line args handling
	const char* filename = argv[1];
	
	filedata = getFileData( (char*)filename, &size);
	
	printf("Loading... %d\r\n", size );
	
    ModPlugFile *f2;	
    f2 = ModPlug_Load(filedata, size);
    if (!f2) {
		printf("could not load %s\n", filename);
		free(filedata); /* ? */
    } 

	printf("Setting up\r\n");
	
	long int pPcmBufferTT = Mxalloc( kBufferSizeHalf, MX_PREFTTRAM );
	long int pPcmBuffer = Mxalloc( 1024*1024*8, MX_STRAM );
	long int pPcmBuffer2 = pPcmBuffer+kBufferSizeHalf;
	
	//memset ( (void*)pPcmBuffer, 0,kBufferSize );
	
	ModPlug_Read(f2,(void*)pPcmBuffer, 1024*1024*8 );

	printf( "rendering done\r\n");

	Setmode		( MODE_STEREO16 );
	Settracks	( 1, 0 );
	Setbuffer( 0, pPcmBuffer, pPcmBuffer+1024*1024*8 );
	Buffoper ( 3 );
	Devconnect	( DMAPLAY, DAC,CLK25M, CLK50K,NO_SHAKE );
	
	SndBufPtr bufferInfo;
		
	Buffer buffer = kBufferSecond;

	 while( 1 )
	 {
	 }
	// 	Buffptr( &bufferInfo );

	// 	if ( 	(long int) bufferInfo.play > pPcmBuffer 
	// 			&& (long int) bufferInfo.play < ( pPcmBuffer + kBufferSizeHalf )
	// 			&& buffer == kBufferSecond
	// 		)
	// 	{
	// 		printf("->");
	// 		int l = ModPlug_Read(f2,(void*)pPcmBufferTT, kBufferSizeHalf );
	// 		memcpy ( (void*)pPcmBuffer,(void*)pPcmBufferTT, kBufferSizeHalf );
	// 		printf("buffer 1 %d\r\n",l);	
	// 		buffer = kBufferFirst;
	// 	}

	// 	if ( 	(long int) bufferInfo.play > pPcmBuffer2 
	// 			&& (long int) bufferInfo.play < ( pPcmBuffer2 + kBufferSizeHalf )
	// 			&& buffer == kBufferFirst
	// 		)
	// 	{
	// 		printf("->");
	// 		int l = ModPlug_Read(f2,(void*)pPcmBufferTT,kBufferSizeHalf);
	// 		memcpy ( (void*)pPcmBuffer2,(void*)pPcmBufferTT, kBufferSizeHalf  );	
	// 		printf("buffer 2 %d\r\n",l);	
	// 		buffer = kBufferSecond;
	// 	}

	// }
		
	return 0;
}